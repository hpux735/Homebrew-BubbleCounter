/*
 *  alarm_clock.c
 *  Lab4
 *
 *  Created by William Dillon on 11/4/10.
 *  Copyright 2010 Oregon State University. All rights reserved.
 *
 */

#define F_CPU 16000000 // cpu speed in hertz 
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#include "alarm_clock.h"
#include "lcd.h"

#pragma mark Shaft encoder Management
/*******************************************************************************
 * Shaft encoder management
 *
 ******************************************************************************/

// This variable it updated by the led servicing ISR.  Clear after checking.
volatile uint8_t knob_state;

/* void update_encoder(uint8_t new_state)
 *
 * This function accepts the new_state variable as the updated latch values
 * from the endoder board.  It keeps the last state and processes any delta.
 * When a encoder turn is detected, a global variable is updated.  The main
 * loop should poll this once per iteration, and clear it when finished.
 * 
 * the format for the global variable is:
 *
 *  Encoder 1  | Encoder 2
 * [f][0][+][-][f][0][+][-]
 *
 * Where 'f' is the flag for a change, the +/- are only one high at a time
 * the '0' is always 0.
 */
#define KNOB1_SIGB 0
#define KNOB1_SIGA 1
#define KNOB2_SIGB 2
#define KNOB2_SIGA 3

static inline void update_encoder(uint8_t new)
{
	static uint8_t old;
	
	/*
	 *                           *Idle    *Idle    *Idle
	 * Clockwise:           A: --|--____--|--____--|--
	 *                      B: --|____----|____----|__
	 *                           *        *        *
	 * Counter-Clockwise:   A: --|--____--|--____--|--
	 *                      B: --|----____|----____|--
	 */
	
	// If the state of signal A is low, B transitions count as turns.
	// This is for encoder 1 (left)
	if (bit_is_clear(new, KNOB1_SIGA)) {
		
		// If the B signal has changed, the knob turned
		if ((new & (1 << KNOB1_SIGB)) ^
			(old & (1 << KNOB1_SIGB))) {
			
			// If the B signal changes low-high, it's a CW turn
			if (bit_is_set(new, KNOB1_SIGB)) {
				knob_state |= KNOB1_FLAG | KNOB1_CW;
			}
			
			// If the B signal changes high-low, it's a CCW turn
			else {
				knob_state |= KNOB1_FLAG | KNOB1_CCW;
			}
		}
	}
	
	// If the state of signal A is low, B transitions count as turns.
	// This is for encoder 2 (right)
	if (bit_is_clear(new, KNOB2_SIGA)) {
		
		// If the B signal has changed, the knob turned
		if ((new & (1 << KNOB2_SIGB)) ^
			(old & (1 << KNOB2_SIGB))) {
			
			// If the B signal changes low-high, it's a CW turn
			if (bit_is_set(new, KNOB2_SIGB)) {
				knob_state |= KNOB2_FLAG | KNOB2_CW;
			}
			
			// If the B signal changes high-low, it's a CCW turn
			else {
				knob_state |= KNOB2_FLAG | KNOB2_CCW;
			}
		}
	}
	
	old = new;
}

uint8_t get_knobs(void)
{
	uint8_t temp = knob_state;
	knob_state = 0;
	return temp;
}

#pragma mark LED Management
/*******************************************************************************
 * LED Management
 *
 ******************************************************************************/

/*  _______
 * |\__A__/|
 * | |   | |
 * |F|   |B|
 * | |___| |
 * |<__G__>|
 * | |   | |
 * |E|   |C|
 * | |___| |
 * |/__D__\|
 */

uint8_t led_table[] = {
	//.GFEDCBA
	0b00111111, // 0
	0b00000110, // 1
	0b01011011, // 2
	0b01001111, // 3
	0b01100110, // 4
	0b01101101, // 5
	0b01111101, // 6
	0b00000111, // 7
	0b01111111, // 8
	0b01101111, // 9
	0b00000000, // <clear>
	0b00000011, // :
	0b00000100,	// PM, no colon
	0b00000111	// PM, colon
};

uint8_t led_test_table[8] = {
	//.GFEDCBA
	0b00000100,
	0b00001100,
	0b00011000,
	0b00110000,
	0b00100001,
	0b00000011,
	0b01000010,
	0b00000000
};

volatile uint8_t bargraph;
volatile uint8_t led_digits[5];
volatile uint8_t buttons;
volatile uint8_t knob_state;
volatile uint8_t decimal_points;

static inline void loop_led(void)
{
	static uint8_t digit;
	
	// First, send the SPI data to the bargraph.  Also, read encoders.  We can
	// do this once per loop because it takes a very short amount of time.
	// Toggle the encoder load signal to latch value
	PORTB = (PORTB & 0x0F) | ((7 << 4) & 0x70);
	PORTB = (PORTB & 0x0F) | ((5 << 4) & 0x70);
	
	SPDR = bargraph;
	
	// Reset the digit counter if necessary.  This is done here because we
	// need to waste some time here anyway.  This way it's useful work.
	if (digit >= 5) digit = 0;
	
	// Then, set the PORTA pins to logic high.  This turns off the LEDs
	// And ensures that there isn't garbage on the LEDs during transition.
	PORTA = 0xFF;
	
	// Wait for the SPI transfer to complete (should be done anyway)
	loop_until_bit_is_set(SPSR, SPIF);
	
	// SPDR now contains the value of the encoder latch
	uint8_t temp = SPDR;
	update_encoder(temp);
	
	// Toggle the bargraph register line to latch value
	PORTB = (PORTB & 0x0F) | ((6 << 4) & 0x70);
	
	// Set the digit selector to the desised digit
	// The "digit select" bits are B:4-6
	PORTB = (PORTB & 0x0F) | ((digit << 4) & 0x70);
	
	// Then, update PORTA
	if (led_digits[digit] > 12) {
		PORTA = ~(led_table[LEDS_OFF]);
	} else {
		PORTA = ~(led_table[led_digits[digit]]);
	}
	
	// Light the decimal place for digit as appropriate
	if (decimal_points & (1 << digit)) {
		PORTA &= ~0x80;
	}
	
	// Update the digit counter for the next time through
	digit++;	
}

/* void led_test(void)
 *
 * This function performs a simple power-on test of the 4-digit 7-segment
 * display.  It takes the animation from led_test_table.
 *
 */
void led_test(void) {
	uint8_t digit, i;
	
	for (digit = 0; digit < 5; digit++) {
		
		// The "digit select" bits are B:4-6
		PORTB = (PORTB & 0x0F) | ((digit << 4) & 0x70);
		
		// If this the colon, deal with it differently
		if (digit == 2) {
			PORTA = ~(0x03);
			_delay_ms(100);
		}
		
		// Otherwise, if it's an other digit deal with it here
		else {
			// Set each segment according to the power-on routine
			for (i = 0; i < (sizeof(led_test_table)); i++) {
				PORTA = ~(led_test_table[i]);
				_delay_ms(100);
			}
		}		
	}
	
	// Turn the segments off
	PORTA = 0xFF;
	
	// Test the bargraph
	for (i = 0; i < 8; i++) {
		// Send a value over SPI
		SPDR = (0xFF << i);
		
		// Wait for it to finish
		while (bit_is_clear(SPSR, SPIF));
		
		// Toggle the register line for the bar graph
		PORTB = (PORTB & 0x0F) | ((6 << 4) & 0x70);
		PORTB = (PORTB & 0x0F) | ((5 << 4) & 0x70);	// Unused output (rising edge)
		
		_delay_ms(100);
	}
}

uint8_t getButtons()
{
	return buttons;
}

void setDigit(uint8_t digit, uint8_t value)
{
	// Perform bounds checking
	if (value > 12) {
		value = LEDS_OFF;
	}
	
//	led_digits[digit] = led_table[value];
	led_digits[digit] = value;
}

uint8_t getDigit(uint8_t digit)
{
	return led_digits[digit];
}

/* void update_bargraph(uint8_t value)
 *
 * This function simply copies the value passed in to the file-scoped bargraph
 * variable.  This is because the bargraph is updated along with the LED
 * scanning routine.
 *
 */
void update_bargraph(uint8_t value)
{
	bargraph = value;
}

/* void update_led(uint16_t value)
 *
 * This function first divides the input value by successively smaller values
 * taking the remainder as the input to the next smaller division.  This takes
 * the binary value and converts it to decimal.  Also, it takes the decimal to
 * 7 segment table values and places it into the digit memory location for each
 * digit.
 *
 * Updates the global "led_digits" variable.
 *
 */
void update_led(uint16_t value, uint8_t zeros) {
	uint8_t digit = 0;
	uint8_t flag = 0;
	
	// Make sure we don't overrun the math
	uint16_t temp = value % 10000;
	
	// Calculate the thousands place
	digit = (uint8_t)(temp / 1000);
	temp = value % 1000;
	if (digit || zeros) {
		led_digits[4] = digit;
		flag = 1;
	} else {
		led_digits[4] = 13;
	}
	
	// Calculate the hundreds place
	digit = (uint8_t)(temp /  100);
	temp = temp  %  100;
	if (digit || flag) {
		led_digits[3] = digit;
		flag = 1;
	} else {
		led_digits[3] = 13;
	}
	
	// Skip digit 2 because it's the colon
	
	// Calculate the tens place
	digit = (uint8_t)(temp /   10);
	temp = temp %   10;
	if (digit || flag) {
		led_digits[1] = digit;
		flag = 1;
	} else {
		led_digits[1] = 13;
	}
	
	// The remainder is ones
	digit = (uint8_t)temp;
	if (digit || flag) {
		led_digits[0] = digit;
	} else {
		led_digits[0] = 13;
	}
}

// This function sets the decimal place LED for a given digit.  The update_led
// routine doesn't respect the decimal place, so it needs to be set each time
// after update_led is run.
void set_dp(uint8_t digit)
{
	if (digit > 4) return;
	
	// Add the decimal place bit (active low) to the digit
	decimal_points |= (1 << digit);
}

void clear_dp(uint8_t digit)
{
	if (digit > 4) return;
	
	// Add the decimal place bit (active low) to the digit
	decimal_points &= ~(1 << digit);
}

#pragma mark Button Management
/*******************************************************************************
 * Button management
 *
 ******************************************************************************/

/* uint8_t doDebounce(uint8_t *state, volatile uint8_t *port, uint8_t pin)
 *
 * This function implements the logic for detecting button transitions for
 * arbitrary ports and bits.  The return value is the debounced value the the
 * given pin.
 *
 * The implementation of this function is inspired by the digital filter 
 * presented in ECE573 at Oregon State University.  It is different because
 * I'm using the single 8-bit variable to store all of the state, rather than a
 * 8 bit accumulator and 8 bit flag.  We're using the range from 0x00 -> 0x7F
 * and bit 7 (0x80) as the flag.
 * 
 * The user of this function must provide a static state variable.  This
 * value encodes the state and history of the pin  Failure to maintain the state
 * would cause erratic behavior.  The port can be any of the PINn 
 * memory-mapped input buffers.  Pin must be between 0 and 7.
 *
 * Because these buttons pull to ground, we'll invert the meaning of the edges
 * in software, 1 = yes is much more natural.
 */
static inline uint8_t 
doDebounce(uint8_t *state, volatile uint8_t *port, uint8_t pin)
{
	uint8_t old  =  *state & 0x7F;
	uint8_t flag = (*state & 0x80)? 1 : 0;
	
	// Digital filter part, value = (old * .75) + (new * .25)
	old -= (old >> 2);								// 1 - (1/4) = .75
	old += ((*port) & (1 << pin))? 0x1F : 0x00;		// if bit set, add .25
	
	// Software schmitt trigger
	// Newly detected rising edge
	if ( (flag == 1) && (old > 0x70) ) {
		flag = 0;		
	}
	
	// Newly detected falling edge
	else if ( (flag == 0) && (old < 0x07) ){
		flag = 1;		
	}
	
	// Update the state variable
	*state = (old & 0x7F) | ((flag & 0x01) << 7);
	
	// Return only the pin state
	return flag;
}

static uint8_t states[8];		// Used internally for button debouncing
volatile uint8_t buttons;		// This always contains current button state
volatile uint8_t button_flag;	// Indicates new button data
volatile uint8_t new_button;	// Contains the newest PORTA read
static inline void scan_buttons(void)
{
	uint8_t old_porta;
	uint8_t old_digit;
	
	// Copy the values from the port and digit
	old_digit = PORTB & 0x70;
	old_porta = PORTA;
	
	// Setup PORTA as an input with pullups enabled
	// First, set the digit selector to some unused setting (we'll use 5)
	PORTB = (PORTB & 0x0F) | (5 << 4);
	PORTA = 0xFF;
	DDRA  = 0x00;
	
	// Next, we switch the demultiplexer to the switch port
	PORTB = (PORTB & 0x0F) | (7 << 4);
	
	button_flag = 1;
	new_button = PINA;
	
	// Return the system to an appropriate state
	PORTB = (PORTB & 0x0F) | old_digit;
	PORTA = old_porta;
	DDRA  = 0xFF;
}

void loop_button(void)
{
	uint8_t i, temp;

	if (button_flag) {
		for (i = 0; i < 8; i++) {
			temp = doDebounce(&(states[i]), &new_button, i);
			if (temp) {
				buttons |= 0x01 << i;
			} else {
				buttons &= ~(0x01 << i);
			}
		}
	}
}

uint8_t get_buttons(void)
{
	return buttons;
}

#pragma mark Peripheral management
/*******************************************************************************
 * Peripheral initialization and management routines
 *
 ******************************************************************************/
#define XDCP_UD  0
#define XDCP_INC 1
#define XDCP_CS  2

void xdcp_increment(void)
{
	// Set the increment line to 1 (negative edge triggered)
	PORTC |= (1 << XDCP_INC);
	_delay_us(1);
	
	// Set the CS line low (active-low)
	PORTC &= ~(1 << XDCP_CS);
	_delay_us(1);
	
	// Set the increment line low to reduce volume setting
	PORTC &= ~(1 << XDCP_INC);	
	_delay_us(1);
	
	// Restore idle state
	PORTC |= (1 << XDCP_CS);
	_delay_us(1);
	
	PORTC |= (1 << XDCP_INC);
}

void xdcp_vol_down(void)
{
	// Set the up/down line to "down"
	PORTC &= ~(1 << XDCP_UD);
	
	// Wait for at least 1 us
	_delay_us(1);
	
	xdcp_increment();
}

void xdcp_vol_up(void)
{
	// Set the up/down line to "down"
	PORTC |= (1 << XDCP_UD);
	
	// Wait for at least 1 us
	_delay_us(1);
	
	xdcp_increment();
}

void xdcp_init(void)
{
	// Setup the digital potentiometer
	//	PORTC  = XDCP_CS | XDCP_INC;
	//	DDRC   = XDCP_CS | XDCP_INC | XDCP_UD; // All XDCP lines are output
	PORTC  = 0x00;
	DDRC   = 0xFF;
	
	// Assure that the volume control is fully down
	uint8_t i;
	for (i = 0; i < 101; i++) {
		xdcp_vol_down();
	}
}

#pragma mark LCD Management
/*******************************************************************************
 * LCD Management
 *
 ******************************************************************************/
char lcd_contents[2][16] = {
	"  LCD  Testing  ",
	"0123456789ABCDEF",
};

void strcpy_lcd(char *output, const char *input)
{
	int i;
	for (i = 0; input[i] != '\0'; i++) {
		output[i] = input[i];
	}
}

char *lcd_line1() { return lcd_contents[0]; }
char *lcd_line2() { return lcd_contents[1]; }

#pragma mark Alarm music
/*******************************************************************************
 * Alarm music code
 *
 ******************************************************************************/
// This is used by the PWMs to generate sine waves
static uint8_t sine_table[] PROGMEM = {
 96,  98, 101, 103, 105, 108, 110, 112, 115, 117, 119, 121, 124, 126, 128, 130,
132, 134, 137, 139, 141, 143, 145, 147, 149, 151, 153, 154, 156, 158, 160, 162,
163, 165, 166, 168, 169, 171, 172, 174, 175, 176, 177, 179, 180, 181, 182, 183,
184, 185, 185, 186, 187, 188, 188, 189, 189, 190, 190, 190, 191, 191, 191, 191,
191, 191, 191, 191, 191, 190, 190, 190, 189, 189, 188, 188, 187, 186, 185, 185,
184, 183, 182, 181, 180, 179, 177, 176, 175, 174, 172, 171, 169, 168, 166, 165,
163, 162, 160, 158, 156, 154, 153, 151, 149, 147, 145, 143, 141, 139, 137, 134,
132, 130, 128, 126, 124, 121, 119, 117, 115, 112, 110, 108, 105, 103, 101,  98,
 96,  94,  91,  89,  87,  84,  82,  80,  77,  75,  73,  71,  68,  66,  64,  62,
 60,  58,  55,  53,  51,  49,  47,  45,  43,  41,  39,  38,  36,  34,  32,  30,
 29,  27,  26,  24,  23,  21,  20,  18,  17,  16,  15,  13,  12,  11,  10,   9,
  8,   7,   7,   6,   5,   4,   4,   3,   3,   2,   2,   2,   1,   1,   1,   1,
  1,   1,   1,   1,   1,   2,   2,   2,   3,   3,   4,   4,   5,   6,   7,   7,
  8,   9,  10,  11,  12,  13,  15,  16,  17,  18,  20,  21,  23,  24,  26,  27,
 29,  30,  32,  34,  36,  38,  39,  41,  43,  45,  47,  49,  51,  53,  55,  58,
 60,  62,  64,  66,  68,  71,  73,  75,  77,  80,  82,  84,  87,  89,  91,  94
};

static uint16_t note_table[] PROGMEM = {
	2194, 2325, 2463, 2611, 2765, 2930,
	3103, 3288, 3484, 3691, 3911, 4143 };

// This variable contains the current delta phase for the sinewave generator
// If it is 0, the phase won't advance and output will stop (with DC blocking)
static volatile uint16_t current_delta;

/* void play_note(uint8_t note, uint8_t octive)
 *
 * This function updates the global current_delta variable given a desired
 * note and octive.  The notes can be any of those defined with the note_table.
 * The octive can be 0 -> 8 inclusive.  A octive above 8 turns off the output.
 *
 */
void play_note(uint8_t note, uint8_t octive)
{
	// Stop the output of the tone generator if out of bounds
	if ((octive > 8) || 
		(note > NOTE_COUNT)) {
		current_delta = 0;
		DDRE &= ~0x08;
	} else {
		// The note table contains the delta for the 8th octive (to store more
		// precision).  To get any other octive, divide it by 2 for each octive
		// lower.  We can do this by right-shifting.
		current_delta = pgm_read_word(&(note_table[note])) >> (8 - octive);
		DDRE |=  0x08;		
	}	
}

ISR(TIMER3_COMPA_vect, ISR_BLOCK)
{
	static uint8_t  next_output;
	static uint16_t current_phase;
	
	OCR3A = next_output;
	
	// If the phase delta is '0' it means no output (50% duty cycle forever)
	if (current_delta == 0) {
		current_phase = 0;
		next_output = pgm_read_word(&sine_table[0]);

	// Otherwise, advance the phase and set the next_output variable for later
	} else {
		current_phase += current_delta;
		if (current_phase &  0x8000) {
			current_phase &= 0x7FFF;
			current_phase += 1;
		}
		
		uint8_t index = (current_phase >> 7);
		next_output = pgm_read_word(&sine_table[index]);
	}
	
}

#pragma mark Timers and Interrupts
/*******************************************************************************
 * User interface interrupt service routine
 *
 * Runs at 512 Hz, or 1.9 ms between invocations
 ******************************************************************************/
ISR(TIMER0_COMP_vect, ISR_NOBLOCK)
{

	// Once per loop, switch digits on the LEDs
	loop_led();
	
	// Once per loop, collect button states
	scan_buttons();
	
	// This variable tracks the which character position is under the cursor
	// on the LCD.  It is actually 2, 4 bit numbers: one for the line and one
	// for the character.
	static uint8_t lcd_counter;	

	// Once per loop, send a character to the LCD
	// Incrememnt the counter and handle wrap-around
	switch (lcd_counter) {
		// First character on the second line
		case 0x10:
			home_line2_nowait();
			lcd_counter++;
			break;

		// Wrap-around to the beginning
		case 0x21:
			cursor_home_nowait();
			lcd_counter = 0;			
			break;
			
		default:
			if (lcd_counter < 0x10) {
				char2lcd_nowait(lcd_contents[0][lcd_counter & 0x0F]);
			} else {
				char2lcd_nowait(lcd_contents[1][(lcd_counter - 1) & 0x0F]);
			}
			lcd_counter++;
			break;
	}

	// Get and update the LED brightness
	if (ADCSRA & (1 << ADIF)) {			// Is a conversion complete?
		OCR2 = ADCH << 1;				// PWM range 0 < OCR2 < 200
		ADCSRA |= (1 << ADSC);			// Start a new conversion
	}
	
}

void init_tcnt0()
{
	// Use the external 32.7 khz oscillator
	ASSR   = (1 << AS0);
	// Enable the clock in CTC mode with no prescaler
	TCCR0  = (1 << CS00) | (1 << WGM01);
	// Set the compare interrupt to 64
	OCR0   = 64;
	// Enable the compare interrupt
	TIMSK |= (1 << OCIE0);
	
	// Intialize the ADC used in the timer 0 OSR
	// Use internal 2.56 reference, left-adjust, and choose ADC0
	ADMUX  = (1 << REFS1) | (1 << REFS0) | (1 << ADLAR);
	ADCSRA = (1 << ADEN) | 0x03;	// Enable and set prescaler to 128
	ADCSRA |= (1 << ADSC);	// Start a new conversion	
}

void init_tcnt2()
{
	// Enable the timer in fast PWM mode with no prescaler
	TCCR2  = (1 << CS20) | (1 << WGM21) | (1 << WGM20);
	// Enable output
	TCCR2 |= (1 << COM21) | (1 << COM20);
	
	// Set default brightness value in the PWM
	OCR2 = 200;
}

// We're using tcnt3 as the PWM
// The fundamental PWM frequency is about 60khz.
void init_tcnt3()
{
	// Enable OC3A PWM output and 8 bit PWM
	TCCR3A  = (1 << COM3A0) | (1 << COM3A1) | (1 << WGM30);
	
	// Finish setting up PWM, and select internal clock, no prescaler
	TCCR3B  = (1 << WGM32) | (1 << CS30);
	
	// Set a starting value for the output compare (mid-value)
	OCR3AL  = 128;
	OCR3AH  = 0;
	
	// Enable the compare interrupt
	ETIMSK |= (1 << OCIE3A);
}
