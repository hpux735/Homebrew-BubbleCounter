/*
 *  lcd.c
 *  Lab2
 *
 *  Created by William Dillon on 10/8/10.
 *  Copyright 2010 Oregon State University. All rights reserved.
 *
 */

// Adapted from:
// misc LCD functions for mega128 board
// refactored by R. Traylor 10.7.09
// http://web.engr.oregonstate.edu/~traylor/ece473/example_code/code/lcd_functions.c


#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>

#include "lcd.h"

void strobe_lcd(void){
	//twiddles bit 3, PORTF creating the enable signal for the LCD
	PORTF |= 0x08;
	PORTF &= ~0x08;
}

/* void send_lcd(uint8_t command, uint8_t data, uint8_t wait)
 *
 * This function writes a pair of octets to the LCD module.  The first
 * octet sets whether the byte is data to be displayed or a command.  A value
 * of 0x00 means command while 0x01 is data.  This function sanitizes this
 * input such that any non-zero value will be interpreted as "data".  The next 
 * octet is the body of the message to the LCD.  The final argument to this
 * function is the mS delay to be executed after the command is sent.  A 
 * zero value here is O.K., and uS delays can be added after the return.
 *
 */
void send_lcd(uint8_t data, uint8_t byte, uint8_t wait)
{
	// command or data
	SPDR = (data)? 0x01 : 0x00;	// Send the proper setting for intent
	while (!(SPSR & 0x80)) {}	// Wait for SPI transfer to complete
	SPDR = byte;				// Send payload
	while (!(SPSR & 0x80)) {}	// Wait for SPI transfer to complete
	strobe_lcd();				//strobe the LCD enable pin
	
	if (wait != 0) {
		_delay_ms(wait);			//obligatory waiting for slow LCD
	}
}

void clear_display(void){
	send_lcd(0x00, 0x01, 2);
}         

void cursor_home(void){
	send_lcd(0x00, 0x02, 1);
}         

void home_line2(void){
	send_lcd(0x00, 0xC0, 1);
}                           

void fill_spaces(void){
	int count;
	for (count=0; count < 16; count++){
		send_lcd(0x01, 0x20, 1);
	}
}  

void char2lcd(char a_char){
	//sends a char to the LCD
	//usage: char2lcd('H');  // send an H to the LCD
	send_lcd(0x01, a_char, 1);
}

void string2lcd(const char *lcd_str){	
	//sends a string to LCD
	uint8_t i;
	for (i = 0; lcd_str[i] != '\0' ; i++) {
		send_lcd(0x01, lcd_str[i], 1);
	}
} 

void set_cursor(uint8_t boolean)
{
	if (boolean) {
		send_lcd(0x00, 0x0E, 1);
	} else {
		send_lcd(0x00, 0x0C, 1);
	}
}

void lcd_init(void){
	int i;

	// Set the strobe pin as output
	PORTF &= ~0x08;
	DDRF  |=  0x08;

	// Set the 
	
	// initalize the LCD to receive data
	_delay_ms(15);

	// do funky initalize sequence 3 times
	for(i=0; i<=2; i++){ 
		send_lcd(0x00, 0x30, 7);
	}
	
	send_lcd(0x00, 0x38, 5);
	send_lcd(0x00, 0x08, 5);
	send_lcd(0x00, 0x01, 5);
	send_lcd(0x00, 0x06, 5);
	send_lcd(0x00, 0x0C, 5);	
} 

void char2lcd_nowait(char a_char)
{
	send_lcd(0x01, a_char, 0);	
}

void clear_display_nowait(void){
	send_lcd(0x00, 0x01, 0);
}         

void cursor_home_nowait(void){
	send_lcd(0x00, 0x02, 0);
}         

void home_line2_nowait(void){
	send_lcd(0x00, 0xC0, 0);
}                           

