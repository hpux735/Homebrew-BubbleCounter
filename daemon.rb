#!/usr/bin/ruby
#  grapher.rb
#  BubbleCounter
#
#  Created by William Dillon on 10/2/11.
#  Copyright 2011 Oregon State University (COAS). All rights reserved.
#

require "rubygems"
require "serialport"
require "RRD"

# create an rrdtool database
rrd_name = "bubbles.rrd"

# A Primary data point (PDP) will be captured every minute
# Later, an average minimum and maximum will be generated using 5 minutes of PDPs
# If we don't get a sample in an hour (3600 seconds) we'll call that unknown (heartbeat)
# We're using the real value given by our bubbles/hour number, so we use GUAGE
# There are 2016 five minute periods in a week and 12096 in 6 weeks
RRD.create(
	rrd_name,
	"--step", "60",
	"DS:bubbles:GAUGE:3600:U:U",
	"RRA:MIN:0.5:5:2016",
	"RRA:AVERAGE:0.5:5:12096",
	"RRA:MAX:0.5:5:12096")

# serial port params
port_str  = "/dev/ttyS1"
baud_rate = 9600
data_bits = 8
stop_bits = 1
parity = SerialPort::NONE

#
logfile = File.new("bubbles.log", "w")

# hopfully the flow control is ok
sp = SerialPort.new(port_str, baud_rate, data_bits, stop_bits, parity)
sp.flow_control = SerialPort::NONE

last_record_seconds = 0
last_graph_seconds = 0
bubbles_per_second = 0

while true do
	record = sp.gets

	logfile.write record
	logfile.flush

	record = record.chomp
	printf("%s: ", record)
	
	record_fields = record.split(",")
	date_fields = record_fields[0].split(":")

# A Day is 86400 seconds
	seconds  = date_fields[0].to_i * 86400
# An hour is 3600 seconds
	seconds += date_fields[1].to_i *  3600
# A minute is 60 seconds
	seconds += date_fields[2].to_i *    60
# Seconds require no conversion
	seconds += date_fields[3].to_i

# Get an interval from the last to this bubble in seconds
	interval = seconds - last_record_seconds
	last_record_seconds = seconds

	if interval == 0
		bubbles_per_second += 1
	else
	# if, in the last second, we counted more than one bubble
	# find the frequency by multiplying the bubbles per second
	# to bubbles per hour
		if bubbles_per_second > 0
			frequency = bubbles_per_second * 3600
			bubbles_per_second = 0
		else
		# Convert the interval into fractions of an hour
			interval = interval / 3600.0

		# Convert the interval to bubbles per hour
			frequency = 1.0 / interval
		end

		printf("%f bubbles per hour\n", frequency)

		RRD.update(rrd_name, "N:#{frequency.to_s}")
#		update_time = seconds + start_time
#		RRD.update(rrd_name, "#{update_time}:#{frequency.to_s}")
	end

# Only generate a new graph if the old one is over 5 minutes old
	graph_interval = seconds - last_graph_seconds
	if graph_interval > 300
		printf("Generating graph, old one is %d seconds old.\n", graph_interval);
		system "./grapher.rb"
		last_graph_seconds = seconds
	end
end
logfile.close
sp.close
