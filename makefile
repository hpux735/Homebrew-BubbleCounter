#  Created by William Dillon on 12/3/10.
#  Copyright 2010
#
#  Released under the TAPR Open Hardware License

PRG            	   = main
LIBS		   = lcd.c uart.c
#HEADERS		   = lcd.h uart.h

OBJ            = $(PRG).o

MCU_TARGET     = atmega128
OPTIMIZE       = -O2    # options are 1, 2, 3, s
CC             = /usr/local/CrossPack-AVR/bin/avr-gcc

override CFLAGS        = -g $(OPTIMIZE) -mmcu=$(MCU_TARGET) $(DEFS)
override LDFLAGS       = -Wl,-Map,$(PRG).map

OBJCOPY        = /usr/local/CrossPack-AVR/bin/avr-objcopy
OBJDUMP        = /usr/local/CrossPack-AVR/bin/avr-objdump

all: $(PRG).elf lst text eeprom

$(PRG).elf: $(OBJ) $(LIBS) $(HEADERS)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ 

clean: 
	rm -rf *.o $(PRG).elf *.bin *.hex *.srec *.bak  
	rm -rf $(PRG)_eeprom.bin $(PRG)_eeprom.hex $(PRG)_eeprom.srec
	rm -rf *.lst *.map 

#setup for for USB programmer
program: $(PRG).hex
	/usr/local/CrossPack-AVR/bin/avrdude -c osuisp2 -P usb -B 100 -B 10 -p m128 -e -U flash:w:$(PRG).hex  -v

lst:  $(PRG).lst

%.lst: %.elf
	$(OBJDUMP) -h -S $< > $@

# Rules for building the .text rom images

text: hex bin srec

hex:  $(PRG).hex
bin:  $(PRG).bin
srec: $(PRG).srec

%.hex: %.elf
	$(OBJCOPY) -j .text -j .data -O ihex $< $@

%.srec: %.elf
	$(OBJCOPY) -j .text -j .data -O srec $< $@

%.bin: %.elf
	$(OBJCOPY) -j .text -j .data -O binary $< $@

# Rules for building the .eeprom rom images

eeprom: ehex ebin esrec

ehex:  $(PRG)_eeprom.hex
ebin:  $(PRG)_eeprom.bin
esrec: $(PRG)_eeprom.srec

%_eeprom.hex: %.elf
	$(OBJCOPY) -j .eeprom --change-section-lma .eeprom=0 -O ihex $< $@

%_eeprom.srec: %.elf
	$(OBJCOPY) -j .eeprom --change-section-lma .eeprom=0 -O srec $< $@

%_eeprom.bin: %.elf
	$(OBJCOPY) -j .eeprom --change-section-lma .eeprom=0 -O binary $< $@
