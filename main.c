/*
 *  main.c
 *  Homebrew bubble counter
 *
 *  Created by William Dillon on 12/3/10.
 *  Copyright 2010
 *
 *  Released under the TAPR Open Hardware License
 *
 */

#define F_CPU 16000000 // cpu speed in hertz 

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <string.h>

#include "lcd.h"
#include "uart.h"

#pragma mark User Interface Logic
/*******************************************************************************
 * User interface logic and LCD constants
 *
 ******************************************************************************/

const char bubbles_title[]  = "    Bubbles!    ";
const char total_bubbles[]  = "Total:     ";
const char hourly_bubbles[] = "Hourly:    ";
const char brewing_timer[]  = "Time ";

volatile uint8_t system_state;

#define STATE_DISP_CYCLE  0x02
#define STATE_LCD_STALE   0x04

#pragma mark Timekeeping
/*******************************************************************************
 * Time keeping logic
 *
 ******************************************************************************/

// Storage for the clock
volatile uint8_t	days;		// Days since beginning
volatile uint8_t	hours;		// Hours since beginning (less days)
volatile uint8_t	mins;	// Minutes since the hour 
volatile uint8_t	secs;	// Seconds since last minute

// Timer one compare interrupt vector
ISR(TIMER1_COMPA_vect, ISR_NOBLOCK)
{
	// Increment and manage time
	secs++;
	
	// Cycle the state of the display every 5 seconds
	if (secs % 5 == 0) {
		system_state = system_state | STATE_DISP_CYCLE;
		PORTB = PORTB ^ 0x20;
	}
	
	// Blink the heartbeat
	PORTB = PORTB ^ 0x10;
	
	// refresh the display
	system_state |= STATE_LCD_STALE;
	
	// Handle the motion of seconds, minutes, hours and days
	if (secs == 60) {
		secs = 0;
		mins++;

		if (mins == 60) {
			mins = 0;
			hours++;

			if (hours == 24) {
				hours = 0;
				days++;
			}
		}		
	}	
}

// Initialize timer 1 to fire once a second
inline void init_tcnt1()
{
	// Normal mode with no output
	TCCR1A = 0x00;

	// osc/256 prescaler
	TCCR1B = (1 << CS12) | (1 << WGM12);

	OCR1A = 62500;
	
	// OCR1 Compare interrupt enable (1 hz)
	TIMSK |= (1 << OCIE1A);
}

// Prints the time to the given buffer.
// Must be at least 12 bytes.
void time2ascii(char *buffer)
{	
	buffer[0] = (days  / 10) + '0';	// Get the 10s place for the days
	buffer[1] = (days  % 10) + '0';	// Get the  1s place for the days
	buffer[2] = ':';
	buffer[3] = (hours / 10) + '0'; // Get the 10s place for the hours
	buffer[4] = (hours % 10) + '0';	// Get the  1s place for the hours
	buffer[5] = ':';
	buffer[6] = (mins  / 10) + '0';	// Get the 10s place for the minutes
	buffer[7] = (mins  % 10) + '0';	// Get the  1s place for the minutes
	buffer[8] = ':';
	buffer[9]  = (secs / 10) + '0';	// Get the 10s place for the seconds
	buffer[10] = (secs % 10) + '0';	// Get the  1s place for the seconds
	buffer[11] = '\0';
}

void int2ascii(uint8_t val, char *buffer)
{
	uint8_t temp;
	temp = val;
	uint8_t flag = 0;
	
	buffer[0] = (temp / 100) + '0';
	temp = temp % 100;

	buffer[1] = (temp / 10) + '0';
	temp = temp % 10;
	
	buffer[2] = temp + '0';
}

#pragma mark utility
/* uint8_t doDebounce(uint8_t *state, volatile uint8_t *port, uint8_t pin)
 *
 * This function implements the logic for detecting button transitions for
 * arbitrary ports and bits.  The return value is the debounced value the the
 * given pin.
 *
 * The implementation of this function is inspired by the digital filter 
 * presented in ECE573 at Oregon State University.  It is different because
 * I'm using the single 8-bit variable to store all of the state, rather than a
 * 8 bit accumulator and 8 bit flag.  We're using the range from 0x00 -> 0x7F
 * and bit 7 (0x80) as the flag.
 * 
 * The user of this function must provide a static state variable.  This
 * value encodes the state and history of the pin  Failure to maintain the state
 * would cause erratic behavior.  The port can be any of the PINn 
 * memory-mapped input buffers.  Pin must be between 0 and 7.
 *
 * Because these buttons pull to ground, we'll invert the meaning of the edges
 * in software, 1 = yes is much more natural.
 */
static inline uint8_t 
doDebounce(uint8_t *state, volatile uint8_t *port, uint8_t pin)
{
	uint8_t old  =  *state & 0x7F;
	uint8_t flag = (*state & 0x80)? 1 : 0;
	
	// Digital filter part, value = (old * .75) + (new * .25)
	old -= (old >> 2);								// 1 - (1/4) = .75
	old += ((*port) & (1 << pin))? 0x1F : 0x00;		// if bit set, add .25
	
	// Software schmitt trigger
	// Newly detected rising edge
	if ( (flag == 1) && (old > 0x70) ) {
		flag = 0;		
	}
	
	// Newly detected falling edge
	else if ( (flag == 0) && (old < 0x07) ){
		flag = 1;		
	}
	
	// Update the state variable
	*state = (old & 0x7F) | ((flag & 0x01) << 7);
	
	// Return only the pin state
	return flag;
}

/* void int16toascii(uint16_t value, uint8_t zeros, char *string)
 *
 * This function converts the input integer into a string of numbers
 * the output is null-terminated.  The passed-in string must be at least 6
 * bytes long.  The zeros field determines whether leading zeros are printed.
 *
 */
void int16toascii(uint16_t value, uint8_t zeros, char *string)
{
	uint8_t digit = 0;
	uint8_t flag = 0;
	
	// Make sure we don't overrun the math
	uint16_t temp = value % 100000;
	
	// Calculate the ten-thousands place
	digit = (uint8_t)(temp / 10000);
	temp = value % 10000;
	if (digit || zeros) {
		string[0] = digit + '0';
		flag = 1;
	} else {
		string[0] = ' ';
	}

	// Calculate the thousands place
	digit = (uint8_t)(temp / 1000);
	temp = value % 1000;
	if (digit || flag) {
		string[1] = digit + '0';
		flag = 1;
	} else {
		string[1] = ' ';
	}

	// Calculate the hundreds place
	digit = (uint8_t)(temp /  100);
	temp = temp  %  100;
	if (digit || flag) {
		string[2] = digit + '0';
		flag = 1;
	} else {
		string[2] = ' ';
	}
	
	// Skip digit 2 because it's the colon
	
	// Calculate the tens place
	digit = (uint8_t)(temp /   10);
	temp = temp %   10;
	if (digit || flag) {
		string[3] = digit + '0';
		flag = 1;
	} else {
		string[3] = ' ';
	}
	
	// The remainder is ones
	digit = (uint8_t)temp;
	if (digit || flag) {
		string[4] = digit + '0';
	} else {
		string[4] = ' ';
	}
	
	string[5] = '\0';
}

#pragma mark Initialization
/*******************************************************************************
 * Initialization stuff
 *
 ******************************************************************************/
#define UART_BAUD 9600
#define UART_UBRR F_CPU/16/UART_BAUD-1

static inline void init()
{
	// Setup the 4-digit 7-segment display
	PORTA  = 0xFF;	// Port A is output, each bit is active-low command
	DDRA   = 0xFF;	// for each segment in the display.
	
	PORTB  = 0x00; // port B initalization for SPI
	DDRB   = 0xF7; // Turn on high LEDs and SS, MOSI, SCLK

	PORTD  = 0x00; // Turn off the PORTD outputs
	DDRD   = 0x00; // Use PORTD as inputs
	DDRE   = 0x01; // Use PORTE 6, 0 as input (bubbles & rs232) 1 output 
	
	PORTF  = 0x00;
	DDRF   = 0x08; // port F bit 3 is enable for LCD
	
	PORTB |= 0x80; // Light a status LED
	
	// Setup timers
	init_tcnt1();
	
	// Setup the USART
	uart_init(UART_UBRR);
	
	// Initialize the SPI interface, for the LCD
	//Master mode, Clock=clk/2, Cycle half phase, Low polarity, MSB first SPCR=0x50;
	SPCR   =  0x50;		// set up SPI mode
	SPSR   =  0x01;		// double speed operation

	// Initialize the USART
	uart_init(UART_UBRR);
	
	// Initialize the LCD display
	lcd_init();
	clear_display();	
}

uint8_t minute_counts[60];
char	number_buffer[6];
char	time_buffer[12];
char	message[20];

#pragma mark main

void send_uart_report(uint16_t serial_number)
{
//	00000000011111111 1 1 1
//	01234567890123456 7 8 9
//	DD:HH:MM:SS,NNNNN\n\r\0
		
	time2ascii(message);
	message[11] = ',';
	int16toascii(serial_number, 1, &message[12]);
	message[17] = '\n';
	message[18] = '\r';
	message[19] = '\0';
	
	uart_transmit_string(message);
}

#define DISPLAY_HOURLY 0
#define DISPLAY_TOTAL  1
#define DISPLAY_TIMER  2

int main()
{
	uint8_t		display_mode = 0;
	uint8_t		bubble_debounce = 0;
	uint16_t	total_bubble_count = 0;
	uint8_t		hour_bubble_count = 0;
	uint8_t		minute_tracker = 0;
	
	// Default to 12 hour display
	system_state = 0;
	
	uint8_t i;
	for (i = 0; i < 60; i++) {
		minute_counts[i] = 0;
	}
	
	// Perform all initialization functions
	init();

	// Copy some strings to the LCD
	cursor_home();
	string2lcd(bubbles_title);

	// Enable global interrupts
	sei();

	while (1) {
		
		// Detect minute change
		if (mins != minute_tracker) {
			// Subtract the oldest minute from the total
			hour_bubble_count -= minute_counts[mins];
			minute_tracker = mins;
		}
				
		// Opto sensor state (inverted)
		uint8_t bubble = (PINE & (1 << 6))? 0 : 1;

		// Copy the inverted state to the LEDs
		if (bubble) {
			PORTB |=  0x40;
		} else {
			PORTB &= ~0x40;
		}
		
		// Detect a falling bubble detection edge (end of bubble)
		if (bubble_debounce && !bubble) {

			// Add the bubble to the counters
			total_bubble_count++;
			hour_bubble_count++;
			minute_counts[minute_tracker] += 1;
			
			// Update the LCD
			system_state |= STATE_LCD_STALE;
			
			// Send an update to the serial port
			send_uart_report(total_bubble_count);
		}
		
		bubble_debounce = bubble;
		
		// Is the LCD content stale?
		if (system_state & STATE_LCD_STALE ||
			system_state & STATE_DISP_CYCLE) {
			
			// Reset the stale flag
			system_state = system_state & ~STATE_LCD_STALE;

			// Cycle the display contents if appropriate
			if (system_state & STATE_DISP_CYCLE) {
				// Reset the cycle flag
				system_state = system_state & ~STATE_DISP_CYCLE;

				display_mode = (display_mode + 1) % 3;
			}

			switch (display_mode) {
				case DISPLAY_TOTAL:
					int16toascii(total_bubble_count, 1, number_buffer);
					
					home_line2();
					string2lcd(total_bubbles);
					string2lcd(number_buffer);
					break;
				case DISPLAY_HOURLY:
					int16toascii(hour_bubble_count, 1, number_buffer);
					
					home_line2();
					string2lcd(hourly_bubbles);
					string2lcd(number_buffer);
					break;
				case DISPLAY_TIMER:
					time2ascii(time_buffer);

					home_line2();
					string2lcd(brewing_timer);
					string2lcd(time_buffer);
					break;
				default:
					break;
			}			
		}		
	}
}
