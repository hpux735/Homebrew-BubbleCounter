#!/usr/bin/ruby
#  grapher.rb
#  BubbleCounter
#
#  Created by William Dillon on 10/2/11.
#  Copyright 2011 Oregon State University (COAS). All rights reserved.
#
# This version the grapher.rb file does not use RRDTool ruby gem.
# The reason for this is that the Raspberry Pi's linux distribution
# (debian) does not work very well with ruby.  Due to this, the
# RRDtool gem doesn't work.  It's easy to get rrdtool working on the
# command line, so I'll just use RRD tool using system calls.
# Normally, using system is a terrible idea, because it's insecure,
# but this application does not use the network directly, and all
# the system calls are restricted in what they can do.

# create an rrdtool database
rrd_name = "bubbles.rrd"

# Generate a graph using the rrd database called out in rrd_name
rrd_graph_command = "rrdtool graph " + \
                    "bubbles.png --title \"Homebrew bubbles per hour\" " + \
                    "--start end-604800s --end now --interlace " + \
                    "--imgformat PNG --width=450 --height=450 " + \
                    "--lower-limit 10 --upper-limit 15000 " + \
                    "--logarithmic --rigid --color CANVAS#000000 " + \
                    "DEF:a=#{rrd_name}:bubbles:AVERAGE " + \
                    "AREA:a#f9b714:\"bubbles per hour\" " + \
                    "LINE3:a#f2f1ed"
printf("%s\n", rrd_graph_command)
system rrd_graph_command

# send the graph to the server
# The ~/.ssh/authorized_hosts file on the remote server must have
# a copy of the local users ssh public key, otherwise it will ask for
# password.  This is obviously not a desired behavior.
result = `scp bubbles.png hpux735@alternet.us.com:/home/hpux735/alternet.us.com/other`
if $? != 0
	puts "Unable to upload graph to server"
    else
	puts "Graph image uploaded successfully"
end
