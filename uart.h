/*
 *  uart.h
 *  M128
 *
 *  Created by William Dillon on 12/3/10.
 *  Copyright 2010 Oregon State University. All rights reserved.
 *
 */

#include <avr/io.h>

void uart_init(uint16_t ubrr);
void uart_transmit(char byte);
void uart_transmit_string(char *string);
