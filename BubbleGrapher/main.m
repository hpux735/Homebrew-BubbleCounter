//
//  main.m
//  BubbleGrapher
//
//  Created by William Dillon on 10/2/11.
//  Copyright (c) 2011 Oregon State University (COAS). All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import <MacRuby/MacRuby.h>

int main(int argc, char *argv[])
{
	return macruby_main("rb_main.rb", argc, argv);
}
