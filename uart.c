/*
 *  uart.c
 *  M128
 *
 *  Created by William Dillon on 12/3/10.
 *  Copyright 2010
 *
 *  Released under the TAPR Open Hardware License
 *
 */

#include "uart.h"

void uart_init(uint16_t ubrr)
{
	// Set baud rate
//	UBRR0 = ubrr;
	UBRR0H = (uint8_t)(ubrr >> 8);
	UBRR0L = (uint8_t)(ubrr);
	
	// Enable the receiver and transmitter
	UCSR0B = (1 << RXEN0) | (1 << TXEN0);
	
	// Frame structure: N81
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00);
}

void uart_transmit(char byte)
{
	// Wait for empty transmit buffer
	loop_until_bit_is_set(UCSR0A, UDRE0);
	
	// Send byte
	UDR0 = byte;
}

void uart_transmit_string(char *string)
{
	for ( ; *string != '\0'; string++) {
		// Wait for empty transmit buffer
		loop_until_bit_is_set(UCSR0A, UDRE0);
		
		// Send byte
		UDR0 = *string;
	}
}
