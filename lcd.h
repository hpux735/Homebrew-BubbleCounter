/*
 *  lcd.h
 *  Lab2
 *
 *  Created by William Dillon on 10/8/10.
 *  Copyright 2010 Oregon State University. All rights reserved.
 *
 */

// Adapted from:
// LCD function header file
// for mega128 board
// R. Traylor 10.7.09
// http://web.engr.oregonstate.edu/~traylor/ece473/example_code/code/lcd_functions.h

#include <avr/io.h>

/* void lcd_init(void);
 *
 * This function performs the necessary initialization for the LCD module.
 *
 * Execute it before attempting to print anything.
 *
 */
void lcd_init(void);

/* void send_lcd(uint8_t command, uint8_t data, uint8_t wait)
 *
 * This function writes a pair of octets to the LCD module.  The first
 * octet sets whether the byte is data to be displayed or is a command.  A value
 * of 0x00 means command while 0x01 is data.  This function sanitizes this
 * input such that any non-zero value will be interpreted as data.  The next 
 * octet is the body of the message to the LCD.  The final argument to this
 * function is the mS delay to be executed after the command is sent.  A 
 * zero value here is O.K., and uS delays can be added after the return.
 *
 */
void send_lcd(uint8_t data, uint8_t byte, uint8_t wait);

/* void string2lcd(char *lcd_str);
 *
 * This function prints a string to the LCD.  The only argument is the string
 * to be printed.  It must be NULL terminated.  For the string to fit on the LCD
 * it probably should also be less than 16 characters long, assuming the cursor
 * is at the home position for the line.
 *
 */
void string2lcd(const char *lcd_str);

/* void char2lcd(char a_char);
 *
 * This function writes a single character to the LCD under the cursor.
 *
 */
void char2lcd(char a_char);
void char2lcd_nowait(char a_char);

/* void set_cursor(uint8_t boolean)
 *
 * This function enables or disables the cursor.  To turn the cursor on send
 * a non-zero value in as boolean.
 *
 */
void set_cursor(uint8_t boolean);

/*
 *
 * The folloing functions are convenience routines for performing common tasks
 * using the LCD.
 *
 */


void cursor_home(void);		// Sets the cursor to column 1, line 1.
void cursor_home_nowait(void);
void home_line2(void);      // Sets the cursor to column 1, line 2.
void home_line2_nowait(void);
void fill_spaces(void);		// 
void clear_display(void);	// Clears the contents of the display
void clear_display_nowait(void);
