/*
 *  alarm_clock.h
 *  Lab4
 *
 *  Created by William Dillon on 11/4/10.
 *  Copyright 2010 Oregon State University. All rights reserved.
 *
 */

#include <avr/io.h>

void spi_init(void);

#define COLON_DIGIT	 2
#define LEDS_OFF	10
#define COLON		11
#define PM			12
#define COLON_PM	13

void led_test(void);
void setDigit(uint8_t digit, uint8_t value);
uint8_t getDigit(uint8_t digit);
void update_led(uint16_t value, uint8_t zeros);
void set_dp(uint8_t digit);
void clear_dp(uint8_t digit);
void update_bargraph(uint8_t value);

void loop_button(void);
uint8_t get_buttons(void);

// Encoder bitfields
#define KNOB1_FLAG 0x80
#define KNOB1_CW   0x20
#define KNOB1_CCW  0x10
#define KNOB2_FLAG 0x08
#define KNOB2_CW   0x02
#define KNOB2_CCW  0x01

uint8_t get_knobs(void);

char *lcd_line1();
char *lcd_line2();
void strcpy_lcd(char *output, const char *input);

// This table contains the delta phase per PWM cycle for the 8th octive
// The "play_note" function computes the "current_delta" to use given
// a desired note and octive.
#define NOTE_COUNT 12
#define NOTE_C	00
#define NOTE_Cs	01
#define NOTE_D	02
#define NOTE_Ds	03
#define NOTE_E	04
#define NOTE_F	05
#define NOTE_Fs	06
#define NOTE_G	07
#define NOTE_Gs	08
#define NOTE_A	09
#define NOTE_As	10
#define NOTE_B	11

// The play_note function can play any note listed above in octives 0-8
// If it is given a note or octive out of bounds, it will slience the output
void play_note(uint8_t note, uint8_t octive);

// These functions initialize the timers
void init_tcnt0(void);
void init_tcnt2(void);
void init_tcnt3(void);

// These function control and initalize the digitally-controlled potentiometer
// that determes the volume control voltage.  It defaults to 0 on startup.
void xdcp_vol_down(void);
void xdcp_vol_up(void);
void xdcp_init(void);
