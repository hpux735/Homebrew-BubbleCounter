#!/usr/bin/ruby
#  grapher.rb
#  BubbleCounter
#
#  Created by William Dillon on 10/2/11.
#  Copyright 2011 Oregon State University (COAS). All rights reserved.
#

require "rubygems"
require "RRD"

# create an rrdtool database
rrd_name = "bubbles.rrd"

# Generate a graph using the rrd database called out in rrd_name
RRD.graph(
        "bubbles.png",
        "--title", "Homebrew bubbles per hour",
        "--start", "end-604800s",
        "--end", "now",
        "--interlace",
        "--imgformat", "PNG",
        "--width=450",
        "--height=450",
        "--lower-limit", "10",
        "--upper-limit", "15000",
        "--logarithmic",
        "--rigid",
        "--color", "CANVAS#000000",
        "DEF:a=#{rrd_name}:bubbles:AVERAGE",
        "AREA:a#f9b714:bubbles per hour",
        "LINE3:a#f2f1ed")

# send the graph to the server
# The ~/.ssh/authorized_hosts file on the remote server must have
# a copy of the local users ssh public key, otherwise it will ask for
# password.  This is obviously not a desired behavior.
result = `scp bubbles.png alternet.us.com:/home/hpux735/alternet.us.com/other`
if $? != 0
	puts "Unable to upload graph to server"
else
	puts "Graph image uploaded successfully"
end

#`./daemon-log.rb`
#result = `./grapher-log.rb`
#puts result
