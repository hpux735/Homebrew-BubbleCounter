#!/usr/bin/ruby
#  grapher.rb
#  BubbleCounter
#
#  Created by William Dillon on 10/2/11.
#  Copyright 2011 Oregon State University (COAS). All rights reserved.
#
# This version the daemon.rb file does not use RRDTool ruby gem.
# The reason for this is that the Raspberry Pi's linux distribution
# (debian) does not work very well with ruby.  Due to this, the
# RRDtool gem doesn't work.  It's easy to get rrdtool working on the
# command line, so I'll just use RRD tool using system calls.
# Normally, using system is a terrible idea, because it's insecure,
# but this application does not use the network directly, and all
# the system calls are restricted in what they can do.

require "rubygems"
require "serialport"

# create an rrdtool database
rrd_name = "bubbles.rrd"

# determine whether the rrd file exists.  If it does, we just want
# to continue using it.  This is useful if this script needs to be
# restarted.
if File.exists?(rrd_name)
    printf("RRD file (%s) exists, appending.\n", rrd_name);
else
    # A Primary data point (PDP) will be captured every minute
    # Later, an average minimum and maximum will be generated using 5 minutes of PDPs
    # If we don't get a sample in an hour (3600 seconds) we'll call that unknown (heartbeat)
    # We're using the real value given by our bubbles/hour number, so we use GUAGE
    # There are 2016 five minute periods in a week and 12096 in 6 weeks
    rrd_create_command = "rrdtool create " + \
                         rrd_name + \
                         " --step 60 " + \
                         "DS:bubbles:GAUGE:3600:U:U " + \
                         "RRA:MIN:0.5:5:2016 " + \
                         "RRA:AVERAGE:0.5:5:12096 " + \
                         "RRA:MAX:0.5:5:12096"

    printf("%s\n", rrd_create_command)
    system rrd_create_command
end

#
logfile = File.new("bubbles.log", "w")

# hopfully the flow control is ok
# serial port params
port_str  = "/dev/ttyUSB0"
baud_rate = 9600
data_bits = 8
stop_bits = 1
parity = SerialPort::NONE

sp = SerialPort.new(port_str, baud_rate, data_bits, stop_bits, parity)
sp.flow_control = SerialPort::NONE
# Use a 30 second timeout for the serial port.  This guarentees that
# even if there is at least on entry in the database per minute.
# This is doubly useful during the end of fermentation.  The actual
# duration seems to be a little off.  No matter what I do, it seems
# to be about 15-20 seconds.  I'll need to keep track of time (for
# updating the graph) a different way.
sp.read_timeout = 500000

# Initialize variables
last_record_seconds = 0
last_graph_seconds = 0
bubbles_per_second = 0
timedout_duration = 0

# Forever loop
while true do
    # Read from the serial port (to get a bubble)
	record = sp.gets

    # Check whether this was a timeout or a bubble
    if record != nil
        timedout_duration = 0

        # Store in the logfile
        logfile.write record
        logfile.flush
        
        # Devide into fields
        record = record.chomp
        printf("%s: ", record)
        
        record_fields = record.split(",")
        date_fields = record_fields[0].split(":")
        
        # A Day is 86400 seconds
        seconds  = date_fields[0].to_i * 86400
        # An hour is 3600 seconds
        seconds += date_fields[1].to_i *  3600
        # A minute is 60 seconds
        seconds += date_fields[2].to_i *    60
        # Seconds require no conversion
        seconds += date_fields[3].to_i
        
        # Get an interval from the last to this bubble in seconds
        interval = seconds - last_record_seconds
        last_record_seconds = seconds

        if interval == 0
            bubbles_per_second += 1
            else
            # if, in the last second, we counted more than one bubble
            # find the frequency by multiplying the bubbles per second
            # to bubbles per hour
            if bubbles_per_second > 0
                frequency = bubbles_per_second * 3600
                bubbles_per_second = 0
                else
                # Convert the interval into fractions of an hour
                interval = interval / 3600.0
                
                # Convert the interval to bubbles per hour
                frequency = 1.0 / interval
            end
            
            printf("%f bubbles per hour\n", frequency)
            rrd_update_command = "rrdtool update " + rrd_name + " N:" + frequency.to_s
            printf("%s\n", rrd_update_command)
            system rrd_update_command
        end

    # This was a timeout, therefore for the period the rate was zero
    else
        timedout_duration += 20
        printf("No bubble in the last 30 seconds.\n")
        rrd_update_command = "rrdtool update " + rrd_name + " N:0"
        system rrd_update_command
        seconds = timedout_duration + last_record_seconds
    end
    
    # Only generate a new graph if the old one is over 5 minutes old
	graph_interval = seconds - last_graph_seconds
	if graph_interval > 300
		printf("Generating graph, old one is %d seconds old.\n", graph_interval)
                system "./grapher-ddr.rb"
		last_graph_seconds = seconds
	end
end
    logfile.close
    sp.close
